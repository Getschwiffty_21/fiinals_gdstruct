#include <iostream>
#include <conio.h>
#include "Queue.h"
#include "Stack.h"
#include "UnorderedArray.h"


using namespace std;

void choice1(Queue<int> queue, Stack<int> stack)
{
	int elementalPush;

	cout << "\nGive element to push: ";
	cin >> elementalPush;
	queue.push(elementalPush);
	stack.push(elementalPush);
}

void choice2(Queue<int> queue, Stack<int> stack)
{
	stack.pop();
	queue.pop();
	cout << "\nQueue: ";
	for (int i = 0; i < queue.getSize(); i++)
	{
		cout << queue[i] << " ";
	}

	cout << "\nStack: ";
	for (int i = 0; i < stack.getSize(); i++)
	{
		cout << stack[i] << " ";
	}
}

void choice3(Queue<int> queue, Stack<int> stack)
{
	cout << "\nQueue: ";
	for (int i = 0; i < queue.getSize(); i++)
	{
		cout << queue[i] << " ";
	}

	cout << endl;
	cout << "Stack: ";
	for (int i = 0; i < stack.getSize(); i++)
	{
		cout << stack[i] << " ";
	}
	queue.clear();
	stack.clear();
}

void choice4()
{
	cout << "...THANK YOU!" << endl;
}

void displayTop(Queue<int> queueArray, Stack<int> stackArray)
{
	cout << "Top of the elements: " << endl;
	cout << "Queue: " << queueArray.top() << endl;
	cout << "Stack: " << stackArray.top() << endl;
}

int main() 
{
	int size;
	cout << "Enter size: ";
	cin >> size;

	Queue<int> queue(size);
	Stack<int> stack(size);

	int choice;

	while (true)
	{
		cout << "What do you want to do?" << endl;
		cout << "1 = Push Elements" << endl;
		cout << "2 = Pop Elements" << endl;
		cout << "3 = Print everything then empty set" << endl;
		cout << "4 = Exit" << endl;
		cout << "CHOICE: ";
		cin >> choice;

		if (choice == 1)
		{
			choice1(queue, stack);
			displayTop(queue, stack);
		}
		
		if (choice == 2)
		{
			choice2(queue, stack);
		}

		if (choice == 3)
		{
			choice3(queue, stack);
		}

		if (choice == 4)
		{
			choice4();
			break;
		}

		_getch();
		system("CLS");
	}

	system("Pause");
}